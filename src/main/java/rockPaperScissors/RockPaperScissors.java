package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    
    public void run() {
        
    	boolean shouldContinue = true;
    	
    	while (shouldContinue) {
    		System.out.println("Let's play round " + roundCounter);
    		roundCounter ++;
        	String humanChoice = userChoice();
        	String computerChoice = randomChoice();
        	
        	if (computerChoice.equalsIgnoreCase(humanChoice)) {
        		System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". It's a tie!");
        	}
        	
        	else if (humanChoice.equalsIgnoreCase("rock")) {
        		if (computerChoice.equalsIgnoreCase("paper")){
        			System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". Computer wins!");
        			computerScore ++;
        		}
        		else if (computerChoice.equalsIgnoreCase("scissors")) {
        			System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". Human wins!");
        			humanScore ++;
        		}
        	}
        	else if (humanChoice.equalsIgnoreCase("paper")) {
        		if (computerChoice.equalsIgnoreCase("scissors")) {
        			System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". Computer wins!");
        			computerScore ++;
        		}
        		else if (computerChoice.equalsIgnoreCase("rock")) {
        			System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". Human wins!");
        			humanScore ++;
        		}
        	}
        	else if (humanChoice.equalsIgnoreCase("scissors")) {
        		if (computerChoice.equalsIgnoreCase("rock")) {
        			System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". Computer wins!");
        			computerScore ++;
        		}
        		else if (computerChoice.equalsIgnoreCase("paper")) {
        			System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". Human wins!");
        			humanScore ++;
        		}
        	}
        	
        	System.out.println("Score: human "+humanScore+", computer "+computerScore);
        	
        	String continueGame = "";
        	while (true) {
	        	continueGame = readInput("Do you wish to continue playing? (y/n)?");
	        	if ((continueGame.equalsIgnoreCase("y")) || (continueGame.equalsIgnoreCase("n"))) {
	        		if (continueGame.equalsIgnoreCase("n")){
	        			shouldContinue = false;
	        			break;
	        		}
	        		else {
	        			break;
	        		}
	        	}
	        	else {
	        		continue;
	        		}
	        	}
        		
        	}
        	
        	System.out.println("Bye bye :)");
    	}
    	
    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    
    public String userChoice() {
    	String humanMove = "";
    	while (true) {
    		humanMove = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
    		if (rpsChoices.contains(humanMove)) {
        		break;
        	}
    		else {
    		continue;
    		}
    	}
    	return humanMove;
    }	
    
	public String randomChoice() {
		int random = (int)(Math.random()*3);
		String compMove = "";
		
		if (random == 0) {
			compMove = "rock";
		}
		else if (random == 1) {
			compMove = "paper";
		}
		else {
			compMove = "scissors";
		}
		return compMove; 
	}
}
